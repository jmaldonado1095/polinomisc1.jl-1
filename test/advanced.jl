using FactCheck
import Polynomial
using PolinomisC1


tol = 1e-7

facts("Fets basics") do

	context("Polinomis irreduibles") do
		@fact arrels(Polynomial.Poly([1,0,1])) => empty
		@fact arrels(Polynomial.Poly([1,0,0,0,1])) => empty
	end

	context("Polinomis de grau senar") do
		# Sempre tenen com mínim una arrel
		ncoefs = 6
		for _=[1:5]
			p = Polynomial.Poly(rand(ncoefs))
			@assert isodd(degree(p))
			@fact arrels(p) => not(empty)
		end 
	end

end



facts("Polinomis llargs i aleatoris") do

	nsamples = 7
	tic()
	for _=[1:5]
		xs = sort(unique(rand(nsamples)))
		p = Polynomial.poly(xs)*100*rand()
		# p te arrels simples
		# Dóna el temps que ha passat entre tic i toc
		ys = arrels(p)
		@fact ys => are_roots_of(p)
		@fact length(ys) => degree(p)
	end
	toc()

end