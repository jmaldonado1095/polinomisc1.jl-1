import Polynomial

## Helpers

degree = p -> length(p) - 1

function are_roots_of(p)
	xs -> all(map(x -> peval(p,x+tol) * peval(p,x-tol) < 0, xs))
end

empty = l -> length(l) == 0


peval = Polynomial.polyval

function randompoly(n)
	p = Polynomial.Poly(rand(n+1))
	if degree(p) == n
		return p
	else
		randompoly(n)
	end
end

function randomroots(n)
	Polynomial.poly(rand(n))
end